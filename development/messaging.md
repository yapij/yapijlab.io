# Messaging

# Messagepack Extensions/Hooks

| extension | pyType            | notes                                       |
| --------- | ----------------- | ------------------------------------------- |
| 99        | `UnknownPyType`   | Any type that is not explicitly serialized. |
| 100       | `List`            |                                             |
| 101       | `Tuple`           |                                             |
| 102       | `Set`             |                                             |
| 104       | `Complex`         |                                             |
| 105       | `Callable`        | i.e. a function                             |
| 106       | `Docstring`       |                                             |
| 107       | `warning`         |                                             |
| 108       | `Exception`       |                                             |
| 109       | `Bool`            |                                             |
| 110       | `Dict`            |                                             |
| 111       | `Str`             |                                             |
| 112       | `Float`           |                                             |
| 113       | `Int`             |                                             |
| 30        | `NpArray`         |                                             |
| 40        | `PandasDataFrame` |                                             |
| 41        | `PandasSeries`    |                                             |
