# Data Structures

## Top-Level: Input, Output, and Status

### Input

```js
{
sessionId:string, // Id of session
uid:string,       // Unique id of command
index:number,     // Index of command in session (first command => 1, second => 2, etc.)
in:string,        // Input to process.
date:Date,        // Date/time processed
output:bool       // If true, is output
}
```

### Output

In `asObject()` form:

```js
{
  sessionId:string,                       // ID of Session
  uid:string,                             // Unique id of command
  type:string,                            // level-1 python type (coarse)
  pyType:string,                          // level-2 python type (precise)
  value:[object, string, number, array],  // Output values (differs by type)
  date:Date,                              // Date/time received.
  output:bool                             // If true, is output.
};
```

### Status

In `asObject()` form:

```js
{
sessionId:string,       // ID of Session
uid:string,             // Unique ID of command
codes:arrayOf(number),  // 2-Array of status codes
names:arrayOf(string),  // 2-Array of status names
date:date               // Date/time received
}
```
