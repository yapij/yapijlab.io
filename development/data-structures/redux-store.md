# Redux Store Structure

# As a part of a larger store

...

# State Objects

## `collections`

...

## `serverMsg`

...

## `config`

The `config` object includes props relevant to the styling of the components. There is one action that can be used to set different props. The goal/value-add of this is that it coordinates settings between different child components from different packages. For example, it coordinates the syntax theme across `react-syntax-highlighter/SyntaxHighlighter`, `react-json-view/ReactJson`, and `react-ace/AceEditor`.

### Actions

There is one action:

```js
function setConfig(key, value) {...}
```

Here, `key` is a particular key to be used to set the `config` object while value is the value to be set.

- `reactJsonView.*`. Set an option in `react-json-view`.
  - _Full set of props._ [docs](https://www.npmjs.com/package/react-json-view#props)
  - _Example._ `setConfig('reactJsonView.iconStyle', 'triangle')`
- `reactSyntaxHighlighter.*` Set an option in `react-syntax-highlighter`.
  - _Full set of props._ [docs](https://www.npmjs.com/package/react-syntax-highlighter#props)
  - _Example._ `setConfig('reactSyntaxHighlighter.wrapLines', false)`
- `reactAce.*` Set an option in `react-ace`.
  - _Full set of props._ [docs](https://github.com/securingsincity/react-ace/blob/master/docs/Ace.md)
  - _Example._ `setConfig('reactAce.showGutter', false)`
- `tabSize`. Set the tabs across all components.
  - _Values._ A positive integer.
  - _Example._ `setConfig('tabSize', 4)`
- `theme`. Set the theme across all components.
  - _Values._ One of `'light'` or `'dark'`
  - _Example._ `setConfig('tabSize', 4)`
- `precision`. Set the numberic precision of numbers. Sets appropriate styling across numbers, dollars, percents, and scientific notation. Used in pandas tables among others.
  - _Values._ A positive integer.
  - _Example._ `setConfig('precision', 6) // format(3216.12421521521) => 3,216.124215`
- `fontSize`. Set the font size (in pixels) across components. (`react-ace` only accepts pixels, which prevents other font size specifications.)
  - _Values._ A positive value.
  - _Example._ `setConfig('fontSize', 6)`
- `all`. Merge an object with the `config` property in the store. This is meant to make it easier for a config file to be loaded from memory. For example, one might store config using [`configstore`](https://www.npmjs.com/package/configstore) on the user's computer then load it back into the store whenever the app opens. See the warning below.
  - _Values._ An object with (optional) keys `[reactJsonView, reactSyntaxHighlighter, reactAce, theme, style, prec]`
  - _Example._ `setConfig('all', {...})`

{% hint style='danger' %}
Do not try to set config manually using `setConfig('all', ...)`. There are various implementation details that are easy to mix up (e.g. setting the theme involves setting different complex props across different keys). Instead, set individual props via `reactAce.*`, `tabSize`, etc.

Probably the only good use of `all` is to reload a previous `config` from memory.
{% endhint %}

{% hint style='warning' %}
There are certain props in each of `reactJsonView`, `reactSyntaxHighlighter`, and `reactAce` that are presumed to be untampered with. For example, `theme`/`style` and `language`. In the current implementation, there is nothing to prevent you from changing these props. However, it will probably not go well.  
{% endhint %}

# Default/Initial State

```js
const initialState = {
  serverMsg: [],
  collections: { default: {} },
  config: {
    reactJsonView: {
      iconStyle: "circle",
      indentWidth: 2,
      collapseStringsAfterLength: true,
      enableClipboard: true,
      displayObjectSize: true,
      displayDataTypes: false,
      sortKeys: false,
      validationMessage: "reactJsonView Validation Error",
      theme: syntaxHighlighting.dark.rjv
    },
    reactSyntaxHighlighter: {
      showLineNumbers: false,
      style: syntaxHighlighting.dark.rsl
    },
    reactAce: {
      fontSize: 9,
      highlightActiveLine: false,
      tabSize: 2,
      enableBasicAutocompletion: false,
      enableLiveAutocompletion: false,
      theme: syntaxHighlighting.dark.ace
    },
    theme: "dark",
    style: { tabSize: 2, fontSize: "9px" },
    prec: {
      prec: 4,
      num: "0,0.[0000]",
      pct: "0,0.[0000]%",
      sgn: "+0,0.[0000]",
      usd: "$0,0.[0000]",
      exp: "0.0000e+0"
    }
  }
};
```
