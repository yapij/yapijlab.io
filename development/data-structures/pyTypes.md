# pyTypes

_JavaScript Objects that emulate python objects._

---

# Abstract Class `PyType`

All pyTypes descend from an abstract class, `PyType`. Generally, unless you are defining a new pyType, there should be no reason to deal with `PyType` directly.

## API

### Attributes

- `sessionId:string`. ID of Session
- `uid:string`. Unique id of command
- `type:string`. level-1 python type (coarse)
- `pyType:string`. level-2 python type (precise)
- `date:Date`. Date/time received.
- `value:[object,array,string,number]`. The value associated with a particular pyType. See full docs for exact fields.
- `output:bool`. If true, is output.

### Methods

- `get value()`. Alias for `this._v`.
- `setSessionId(id)`. Sets the session id associated with the object.
- `get asObject()`. Converts the es6 class to a plain object that is suitable for, e.g., redux.
- `fromObject(obj)`. Takes a plain object and sets the properties of the object using this object.

## Creating a new pyType

New pyTypes can be created by extending `PyType`. See the example below as well as `yapij-js/unpackers/pyTypes.js`:

```js
import { PyType } from "yapij-js/unpackers/pyTypes.js";

class NpArray extends PyType {
  constructor(uid, { b, s, t }) {
    super(uid, "np.ndarray", "np.ndarray");
    this._v = { b, s, t };
  }

  // Optional accessors to elements.
  get values() {
    return this._v.b;
  }

  get shape() {
    return this._v.s;
  }

  get dtype() {
    return this._v.t;
  }

  inspect() {
    return `NpArray [ ... ] (${this._v.t}, ${this._v.s})`;
  }
}
```

{% hint style='warning' %}
There is currently no method for adding pyTypes to the `yapij-js` package (e.g. as a plugin). Therefore, this is only relevant for those who are extending or forking `yapij-js` directly.
{% endhint %}

# Particular pyTypes

## Built-in Types

### `Str`

| extends        | pyType | type  | Additional Methods |
| -------------- | ------ | ----- | ------------------ |
| `BuiltInTypes` | `Str`  | `Str` |                    |

**_Values._** (string) The string

### `Dict`

| extends        | pyType | type   | Additional Methods |
| -------------- | ------ | ------ | ------------------ |
| `BuiltInTypes` | `Dict` | `Dict` |                    |

**_Values._** (object) The dict.

### `Float`

| extends        | pyType  | type    | Additional Methods |
| -------------- | ------- | ------- | ------------------ |
| `BuiltInTypes` | `Float` | `Float` |                    |

**_Values._** (number) The float.

### `Int`

| extends        | pyType | type  | Additional Methods |
| -------------- | ------ | ----- | ------------------ |
| `BuiltInTypes` | `Int`  | `Int` |                    |

**_Values._** (number) The int.

### `Bool`

| extends        | pyType | type   | Additional Methods |
| -------------- | ------ | ------ | ------------------ |
| `BuiltInTypes` | `Bool` | `Bool` |                    |

**_Values._** (boolean) The boolean

### `List`

| extends    | pyType | type   | Additional Methods |
| ---------- | ------ | ------ | ------------------ |
| `ListLike` | `List` | `List` |                    |

**_Values._** (array) Members of list.

### `PySet`

| extends    | pyType | type  | Additional Methods |
| ---------- | ------ | ----- | ------------------ |
| `ListLike` | `Set`  | `Set` |                    |

**_Values._** (array) Members of set.

{% hint style='warning' %}
The `PySet` object only tells that the underlying python object is a set. This object is _not_ immutable or have any of the properties of python sets.
{% endhint %}

### `Tuple`

| extends    | pyType  | type    | Additional Methods |
| ---------- | ------- | ------- | ------------------ |
| `ListLike` | `Tuple` | `Tuple` |                    |

**_Values._** (array) Members of tuple.

{% hint style='warning' %}
A similar note here as for `PySet`.
{% endhint %}

### `Complex`

| extends  | pyType  | type    | Additional Methods |
| -------- | ------- | ------- | ------------------ |
| `pyType` | `Tuple` | `Tuple` | `imag`, `real`     |

**_Values._** (Object)

- `r:number`: Real part of number
- `i:number`: Complex part of number

### `Callable`

| extends  | pyType     | type       | Additional Methods |
| -------- | ---------- | ---------- | ------------------ |
| `pyType` | `Callable` | `Callable` |                    |

**_Values._** (string) String returned by python from `callable.__str__`. Usually something like a signature or function name.

### `Exception`

| extends  | pyType      | type                                          | Additional Methods                  |
| -------- | ----------- | --------------------------------------------- | ----------------------------------- |
| `pyType` | `Exception` | (Specific exception type, e.g. `SyntaxError`) | `traceback`, `tb`, `lineno`, `args` |

**_Values._** (Object)

- `tb:string`: Traceback returned by exception
- `lineno:number`: Line number in file where occurred.
- `args:any`: Other args

### `Warning`

| extends  | pyType      | type                                        | Additional Methods                |
| -------- | ----------- | ------------------------------------------- | --------------------------------- |
| `pyType` | `Exception` | (Specific warning type, e.g. `UserWarning`) | `file`, `lineno`, `msg`, `source` |

**_Values._** (Object)

- `file:string`: File where warning arose.
- `lineno:number`: Line number in file where occurred.
- `msg:string`: Warning message.
- `source:string`: Info about where warning coming up.

## NumPy

### `NpArray`

| extends  | pyType          | type            | Additional Methods         |
| -------- | --------------- | --------------- | -------------------------- |
| `pyType` | `numpy.ndarray` | `numpy.ndarray` | `values`, `shape`, `dtype` |

**_Values._** (Object)

- `b:array`: Values of array.
- `s:array[number,number]`: Shape of array.
- `t:string`: NumPy type of array values.

## Pandas

### `PandasDataFrame`

| extends               | pyType             | type               | Additional Methods |
| --------------------- | ------------------ | ------------------ | ------------------ |
| `PandasAbstractFrame` | `pandas.DataFrame` | `pandas.DataFrame` |                    |

**_Values._** (Same as `PandasAbstractFrame`)

### `PandasSeries`

| extends               | pyType          | type            | Additional Methods |
| --------------------- | --------------- | --------------- | ------------------ |
| `PandasAbstractFrame` | `pandas.Series` | `pandas.Series` |                    |

**_Values._** (Same as `PandasAbstractFrame`)

## Special Types

These types provide handling for all other cases.

### `DocString`

| extends  | pyType      | type        | Additional Methods                                                  |
| -------- | ----------- | ----------- | ------------------------------------------------------------------- |
| `pyType` | `Docstring` | `Docstring` | `objType`,`name`, `signature`, `dir`, `module`, `file`, `docstring` |

When `help` is called in the python process, this returns a detailed object. The attribute `str` will format the output similarly to python and iPython shells.

### `UnknownPyType`

| extends  | pyType          | type                       | Additional Methods |
| -------- | --------------- | -------------------------- | ------------------ |
| `pyType` | `UnknownPyType` | (Type returned by process) | `str`              |

If python process lacks a handler for a type, it is processed here. The `type` will be the actual type of the object.

### `UnhandledType`

| extends  | pyType          | type            | Additional Methods |
| -------- | --------------- | --------------- | ------------------ |
| `pyType` | `UnhandledType` | `UnhandledType` |                    |

If output is received but it is not recognizeable as a pre-specified packer type, it is filed as an `UnhandledType`. If these types are popping up, there is probably an IPC error.

## Meta Types

This types provide templates that are extended by particular

### `ListLike`

| extends  | pyType             | type               | Additional Methods |
| -------- | ------------------ | ------------------ | ------------------ |
| `pyType` | (From Constructor) | (From Constructor) | `values`, `length` |

For list-like objects (list, tuple, set, etc.).

### `BuiltInTypes`

| extends  | pyType             | type               | Additional Methods |
| -------- | ------------------ | ------------------ | ------------------ |
| `pyType` | (From Constructor) | (From Constructor) |                    |

For simple built-ins like str, int, float, etc.

### `PandasAbstractFrame`

| extends  | pyType             | type               | Additional Methods                    |
| -------- | ------------------ | ------------------ | ------------------------------------- |
| `pyType` | (From Constructor) | (From Constructor) | `values`, `index`, `columns`, `shape` |

**_Values._** (Object)

- `d:array`: Values of underlying data array.
- `i:array[..[number,string]]`: Indices/row names of
- `c:array[...string]`: Column names/name of series.
- `s:array[number,number]`: Shape of object.

_Values are identical across children._

Getter method `values` differs from `value` in that `values` returns the underlying data array while `value` returns `this._v`.

# Examples

## A script to get a full set of examples

```js
var { Server } = require("./src/index.js");

const outFile =
  "C:/Users/us57144/Dropbox/grantThornton/projects/macro_model/yapij/yapij-re/devMode/samples/samplePyTypes.js";

// Set up the python script
var script = `
import warnings
import contextlib
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

fig = plt.figure()
plt.plot(np.arange(0,np.pi*3,.1), np.sin(np.arange(0,np.pi*3,.1)))

df = pd.DataFrame(np.random.randn(4,3), columns=['a', 'b', 'c'])

warnings.warn('This is a warning')

print(1)
print(1.23)
print(1.23 + 3j)

print(iter((1,2,3)))

print([1,2,"a", 5])
print((1,2,"a", 5))
print(range(5))

print('a')

print(b'a')
print(bytearray(b'abcd'))
print(memoryview(b'abcdefg'))

print({1,2,"a", 5})
print(frozenset({1,2,3}))

print({"g": 5, "h": "a"})

print(contextlib.redirect_stdout)

print(pd)
print(np.max)
print(df.max)
print(compile('1+1', '<str>', 'eval'))
print(type(1))
print(None)
print(...)
print(NotImplemented)
print(True)

print(np.random.randn(4,3))
print(pd.Series(np.random.randn(3)))
print(df)
print(fig)`;

// Initialize server
var s = new Server();

s.on("session.status", (th, msg) => null);
s.on("session.output", (th, msg) => null);
s.on("session.input", (th, msg) => null);

// Start the server
var ss = s.start().then(
  () => {
    console.log("Started");
  },
  e => {
    throw e;
  }
);

var main = s.exec(script);
var exc = s.exec('raise SyntaxError("This is an error!")');

var ex = "";
for (let ii = 0; ii < main.out.length; ii++) {
  if (!(main.out[ii]._v == "\n" || main.out[ii]._v == " ")) {
    ex += `\n\nexport const ${main.out[ii].pyType} = ${JSON.stringify(
      main.out[ii].asObject,
      null,
      2
    )};`;
  }
}

ex += `\n\nexport const ${exc.out[0].pyType} = ${JSON.stringify(
  exc.out[0].asObject,
  null,
  2
)};`;

ex = ex.replace(/"date": "([\w\-\:\.]+)",/gi, '"date": new Date("$1"),');

if (outFile) {
  const fs = require("fs");
  fs.writeFile(outFile, ex, err => {
    if (err) throw err;
  });
}

console.log(ex);
```

## Examples of pyTypes as objects

```js
export const Int = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "int",
  pyType: "Int",
  value: 1,
  date: new Date("2018-11-09T19:13:39.173Z"),
  output: true
};

export const Float = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "float",
  pyType: "Float",
  value: 1.23,
  date: new Date("2018-11-09T19:13:39.173Z"),
  output: true
};

export const Complex = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "complex",
  pyType: "Complex",
  value: {
    r: 1.23,
    i: 3
  },
  date: new Date("2018-11-09T19:13:39.173Z"),
  output: true
};

export const Iterator = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "iter",
  pyType: "Iterator",
  value: "<tuple_iterator object at 0x000001F411B37940>",
  date: new Date("2018-11-09T19:13:39.173Z"),
  output: true
};

export const List = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "list",
  pyType: "List",
  value: [1, 2, "a", 5],
  date: new Date("2018-11-09T19:13:39.173Z"),
  output: true
};

export const Tuple = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "tuple",
  pyType: "Tuple",
  value: [1, 2, "a", 5],
  date: new Date("2018-11-09T19:13:39.173Z"),
  output: true
};

export const Range = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "range",
  pyType: "Range",
  value: "range(0, 5)",
  date: new Date("2018-11-09T19:13:39.173Z"),
  output: true
};

export const Str = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "str",
  pyType: "Str",
  value: "a",
  date: new Date("2018-11-09T19:13:39.173Z"),
  output: true
};

export const Bytes = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "bytes",
  pyType: "Bytes",
  value: "b'a'",
  date: new Date("2018-11-09T19:13:39.173Z"),
  output: true
};

export const ByteArray = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "bytearray",
  pyType: "ByteArray",
  value: "bytearray(b'abcd')",
  date: new Date("2018-11-09T19:13:39.189Z"),
  output: true
};

export const MemoryView = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "memoryview",
  pyType: "MemoryView",
  value: {
    l: [97, 98, 99, 100, 101, 102, 103],
    s: [7],
    b: 7
  },
  date: new Date("2018-11-09T19:13:39.189Z"),
  output: true
};

export const PySet = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "set",
  pyType: "PySet",
  value: [1, 2, 5, "a"],
  date: new Date("2018-11-09T19:13:39.189Z"),
  output: true
};

export const FrozenSet = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "frozenset",
  pyType: "FrozenSet",
  value: [1, 2, 3],
  date: new Date("2018-11-09T19:13:39.189Z"),
  output: true
};

export const Dict = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "dict",
  pyType: "Dict",
  value: {
    g: 5,
    h: "a"
  },
  date: new Date("2018-11-09T19:13:39.189Z"),
  output: true
};

export const ContextManager = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "ContextManager",
  pyType: "ContextManager",
  value: "<class 'contextlib.redirect_stdout'>",
  date: new Date("2018-11-09T19:13:39.189Z"),
  output: true
};

export const Module = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "module",
  pyType: "Module",
  value: {
    n: "pandas",
    dir: [
      "Categorical",
      "CategoricalIndex",
      "DataFrame",
      "DateOffset",
      "DatetimeIndex",
      "ExcelFile",
      "ExcelWriter",
      "Expr",
      "Float64Index",
      "Grouper",
      "HDFStore",
      "Index",
      "IndexSlice",
      "Int64Index",
      "Interval",
      "IntervalIndex",
      "MultiIndex",
      "NaT",
      "Panel",
      "Period",
      "PeriodIndex",
      "RangeIndex",
      "Series",
      "SparseArray",
      "SparseDataFrame",
      "SparseSeries",
      "Term",
      "TimeGrouper",
      "Timedelta",
      "TimedeltaIndex",
      "Timestamp",
      "UInt64Index",
      "WidePanel",
      "_DeprecatedModule",
      "__builtins__",
      "__cached__",
      "__doc__",
      "__docformat__",
      "__file__",
      "__loader__",
      "__name__",
      "__package__",
      "__path__",
      "__spec__",
      "__version__",
      "_hashtable",
      "_lib",
      "_libs",
      "_np_version_under1p10",
      "_np_version_under1p11",
      "_np_version_under1p12",
      "_np_version_under1p13",
      "_np_version_under1p14",
      "_np_version_under1p15",
      "_tslib",
      "_version",
      "api",
      "bdate_range",
      "compat",
      "concat",
      "core",
      "crosstab",
      "cut",
      "date_range",
      "datetime",
      "datetools",
      "describe_option",
      "errors",
      "eval",
      "factorize",
      "get_dummies",
      "get_option",
      "get_store",
      "groupby",
      "infer_freq",
      "interval_range",
      "io",
      "isna",
      "isnull",
      "json",
      "lib",
      "lreshape",
      "match",
      "melt",
      "merge",
      "merge_asof",
      "merge_ordered",
      "notna",
      "notnull",
      "np",
      "offsets",
      "option_context",
      "options",
      "pandas",
      "parser",
      "period_range",
      "pivot",
      "pivot_table",
      "plot_params",
      "plotting",
      "pnow",
      "qcut",
      "read_clipboard",
      "read_csv",
      "read_excel",
      "read_feather",
      "read_fwf",
      "read_gbq",
      "read_hdf",
      "read_html",
      "read_json",
      "read_msgpack",
      "read_parquet",
      "read_pickle",
      "read_sas",
      "read_sql",
      "read_sql_query",
      "read_sql_table",
      "read_stata",
      "read_table",
      "reset_option",
      "scatter_matrix",
      "set_eng_float_format",
      "set_option",
      "show_versions",
      "test",
      "testing",
      "timedelta_range",
      "to_datetime",
      "to_msgpack",
      "to_numeric",
      "to_pickle",
      "to_timedelta",
      "tools",
      "tseries",
      "tslib",
      "unique",
      "util",
      "value_counts",
      "wide_to_long"
    ],
    doc:
      '\npandas - a powerful data analysis and manipulation library for Python\n=====================================================================\n\n**pandas** is a Python package providing fast, flexible, and expressive data\nstructures designed to make working with "relational" or "labeled" data both\neasy and intuitive. It aims to be the fundamental high-level building block for\ndoing practical, **real world** data analysis in Python. Additionally, it has\nthe broader goal of becoming **the most powerful and flexible open source data\nanalysis / manipulation tool available in any language**. It is already well on\nits way toward this goal.\n\nMain Features\n-------------\nHere are just a few of the things that pandas does well:\n\n  - Easy handling of missing data in floating point as well as non-floating\n    point data.\n  - Size mutability: columns can be inserted and deleted from DataFrame and\n    higher dimensional objects\n  - Automatic and explicit data alignment: objects can be explicitly aligned\n    to a set of labels, or the user can simply ignore the labels and let\n    `Series`, `DataFrame`, etc. automatically align the data for you in\n    computations.\n  - Powerful, flexible group by functionality to perform split-apply-combine\n    operations on data sets, for both aggregating and transforming data.\n  - Make it easy to convert ragged, differently-indexed data in other Python\n    and NumPy data structures into DataFrame objects.\n  - Intelligent label-based slicing, fancy indexing, and subsetting of large\n    data sets.\n  - Intuitive merging and joining data sets.\n  - Flexible reshaping and pivoting of data sets.\n  - Hierarchical labeling of axes (possible to have multiple labels per tick).\n  - Robust IO tools for loading data from flat files (CSV and delimited),\n    Excel files, databases, and saving/loading data from the ultrafast HDF5\n    format.\n  - Time series-specific functionality: date range generation and frequency\n    conversion, moving window statistics, moving window linear regressions,\n    date shifting and lagging, etc.\n'
  },
  date: new Date("2018-11-09T19:13:39.189Z"),
  output: true
};

export const Callable = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "callable",
  pyType: "Callable",
  value: {
    n: "amax",
    s: "<function amax at 0x000001F40FAEAEA0>"
  },
  date: new Date("2018-11-09T19:13:39.189Z"),
  output: true
};

export const Method = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "method",
  pyType: "Method",
  value: {
    n: "max",
    s:
      "<bound method DataFrame.max of           a         b         c\n0  0.319831 -0.059300  0.692662\n1  0.484981 -0.280187  2.064612\n2 -0.035997  0.552478 -0.826798\n3 -1.351537 -0.445281 -0.381492>"
  },
  date: new Date("2018-11-09T19:13:39.204Z"),
  output: true
};

export const Code = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "Code",
  pyType: "Code",
  value: '<code object <module> at 0x000001F411B66420, file "<str>", line 1>',
  date: new Date("2018-11-09T19:13:39.204Z"),
  output: true
};

export const Type = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "type",
  pyType: "Type",
  value: "<class 'int'>",
  date: new Date("2018-11-09T19:13:39.204Z"),
  output: true
};

export const NoneType = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "None",
  pyType: "NoneType",
  value: "None",
  date: new Date("2018-11-09T19:13:39.204Z"),
  output: true
};

export const Ellipsis = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "ellipsis",
  pyType: "Ellipsis",
  value: "...",
  date: new Date("2018-11-09T19:13:39.204Z"),
  output: true
};

export const NotImplemented = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "NotImplemented",
  pyType: "NotImplemented",
  value: "NotImplemented",
  date: new Date("2018-11-09T19:13:39.204Z"),
  output: true
};

export const Bool = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "bool",
  pyType: "Bool",
  value: true,
  date: new Date("2018-11-09T19:13:39.204Z"),
  output: true
};

export const NpArray = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "numpy.ndarray",
  pyType: "NpArray",
  value: {
    values: [
      [0.6948521930581089, 0.08245255912669233, -2.4087481285008683],
      [0.5221359653590188, -1.3272575190130476, 0.9612348635606618],
      [0.899421369444219, 1.3748677169863266, 1.209352685283758],
      [-2.0905962313322592, 1.6097380516745405, -0.41523735012084784]
    ],
    shape: [4, 3],
    dtype: "float64"
  },
  date: new Date("2018-11-09T19:13:39.204Z"),
  output: true
};

export const PandasSeries = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "pandas.Series",
  pyType: "PandasSeries",
  value: {
    d: [0.691473538282211, -1.1512948650158192, 2.7940517960441986],
    i: {
      v: ["0", "1", "2"],
      t: "RangeIndex"
    },
    c: "None",
    s: [3]
  },
  date: new Date("2018-11-09T19:13:39.204Z"),
  output: true
};

export const PandasDataFrame = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "pandas.DataFrame",
  pyType: "PandasDataFrame",
  value: {
    d: [
      [
        0.3198306816710428,
        0.48498110717769044,
        -0.0359969367823809,
        -1.3515371869222568
      ],
      [
        -0.059299773886911884,
        -0.28018684722932885,
        0.5524780147547433,
        -0.4452809100699413
      ],
      [
        0.6926623265875399,
        2.0646117368092898,
        -0.8267979730680277,
        -0.38149179911950704
      ]
    ],
    i: {
      v: ["0", "1", "2", "3"],
      t: "RangeIndex"
    },
    c: {
      v: ["a", "b", "c"],
      t: ["f", "f", "f"]
    },
    s: [4, 3]
  },
  date: new Date("2018-11-09T19:13:39.204Z"),
  output: true
};

export const Figure = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "matplotlib.figure.Figure",
  pyType: "Figure",
  value:
    '<?xml version="1.0" encoding="utf-8" standalone="no"?>\n<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"\n  "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">\n<!-- Created with matplotlib (https://matplotlib.org/) -->\n<svg height="345.6pt" version="1.1" viewBox="0 0 460.8 345.6" width="460.8pt" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\n <defs>\n  <style type="text/css">\n*{stroke-linecap:butt;stroke-linejoin:round;}\n  </style>\n </defs>\n <g id="figure_1">\n  <g id="patch_1">\n   <path d="M 0 345.6 \nL 460.8 345.6 \nL 460.8 0 \nL 0 0 \nz\n" style="fill:#ffffff;"/>\n  </g>\n  <g id="axes_1">\n   <g id="patch_2">\n    <path d="M 57.6 307.584 \nL 414.72 307.584 \nL 414.72 41.472 \nL 57.6 41.472 \nz\n" style="fill:#ffffff;"/>\n   </g>\n   <g id="matplotlib.axis_1">\n    <g id="xtick_1">\n     <g id="line2d_1">\n      <defs>\n       <path d="M 0 0 \nL 0 3.5 \n" id="mf0090338b1" style="stroke:#000000;stroke-width:0.8;"/>\n      </defs>\n      <g>\n       <use style="stroke:#000000;stroke-width:0.8;" x="73.832727" xlink:href="#mf0090338b1" y="307.584"/>\n      </g>\n     </g>\n     <g id="text_1">\n      <!-- 0 -->\n      <defs>\n       <path d="M 31.78125 66.40625 \nQ 24.171875 66.40625 20.328125 58.90625 \nQ 16.5 51.421875 16.5 36.375 \nQ 16.5 21.390625 20.328125 13.890625 \nQ 24.171875 6.390625 31.78125 6.390625 \nQ 39.453125 6.390625 43.28125 13.890625 \nQ 47.125 21.390625 47.125 36.375 \nQ 47.125 51.421875 43.28125 58.90625 \nQ 39.453125 66.40625 31.78125 66.40625 \nz\nM 31.78125 74.21875 \nQ 44.046875 74.21875 50.515625 64.515625 \nQ 56.984375 54.828125 56.984375 36.375 \nQ 56.984375 17.96875 50.515625 8.265625 \nQ 44.046875 -1.421875 31.78125 -1.421875 \nQ 19.53125 -1.421875 13.0625 8.265625 \nQ 6.59375 17.96875 6.59375 36.375 \nQ 6.59375 54.828125 13.0625 64.515625 \nQ 19.53125 74.21875 31.78125 74.21875 \nz\n" id="DejaVuSans-48"/>\n      </defs>\n      <g transform="translate(70.651477 322.182437)scale(0.1 -0.1)">\n       <use xlink:href="#DejaVuSans-48"/>\n      </g>\n     </g>\n    </g>\n    <g id="xtick_2">\n     <g id="line2d_2">\n      <g>\n       <use style="stroke:#000000;stroke-width:0.8;" x="142.908162" xlink:href="#mf0090338b1" y="307.584"/>\n      </g>\n     </g>\n     <g id="text_2">\n      <!-- 2 -->\n      <defs>\n       <path d="M 19.1875 8.296875 \nL 53.609375 8.296875 \nL 53.609375 0 \nL 7.328125 0 \nL 7.328125 8.296875 \nQ 12.9375 14.109375 22.625 23.890625 \nQ 32.328125 33.6875 34.8125 36.53125 \nQ 39.546875 41.84375 41.421875 45.53125 \nQ 43.3125 49.21875 43.3125 52.78125 \nQ 43.3125 58.59375 39.234375 62.25 \nQ 35.15625 65.921875 28.609375 65.921875 \nQ 23.96875 65.921875 18.8125 64.3125 \nQ 13.671875 62.703125 7.8125 59.421875 \nL 7.8125 69.390625 \nQ 13.765625 71.78125 18.9375 73 \nQ 24.125 74.21875 28.421875 74.21875 \nQ 39.75 74.21875 46.484375 68.546875 \nQ 53.21875 62.890625 53.21875 53.421875 \nQ 53.21875 48.921875 51.53125 44.890625 \nQ 49.859375 40.875 45.40625 35.40625 \nQ 44.1875 33.984375 37.640625 27.21875 \nQ 31.109375 20.453125 19.1875 8.296875 \nz\n" id="DejaVuSans-50"/>\n      </defs>\n      <g transform="translate(139.726912 322.182437)scale(0.1 -0.1)">\n       <use xlink:href="#DejaVuSans-50"/>\n      </g>\n     </g>\n    </g>\n    <g id="xtick_3">\n     <g id="line2d_3">\n      <g>\n       <use style="stroke:#000000;stroke-width:0.8;" x="211.983598" xlink:href="#mf0090338b1" y="307.584"/>\n      </g>\n     </g>\n     <g id="text_3">\n      <!-- 4 -->\n      <defs>\n       <path d="M 37.796875 64.3125 \nL 12.890625 25.390625 \nL 37.796875 25.390625 \nz\nM 35.203125 72.90625 \nL 47.609375 72.90625 \nL 47.609375 25.390625 \nL 58.015625 25.390625 \nL 58.015625 17.1875 \nL 47.609375 17.1875 \nL 47.609375 0 \nL 37.796875 0 \nL 37.796875 17.1875 \nL 4.890625 17.1875 \nL 4.890625 26.703125 \nz\n" id="DejaVuSans-52"/>\n      </defs>\n      <g transform="translate(208.802348 322.182437)scale(0.1 -0.1)">\n       <use xlink:href="#DejaVuSans-52"/>\n      </g>\n     </g>\n    </g>\n    <g id="xtick_4">\n     <g id="line2d_4">\n      <g>\n       <use style="stroke:#000000;stroke-width:0.8;" x="281.059033" xlink:href="#mf0090338b1" y="307.584"/>\n      </g>\n     </g>\n     <g id="text_4">\n      <!-- 6 -->\n      <defs>\n       <path d="M 33.015625 40.375 \nQ 26.375 40.375 22.484375 35.828125 \nQ 18.609375 31.296875 18.609375 23.390625 \nQ 18.609375 15.53125 22.484375 10.953125 \nQ 26.375 6.390625 33.015625 6.390625 \nQ 39.65625 6.390625 43.53125 10.953125 \nQ 47.40625 15.53125 47.40625 23.390625 \nQ 47.40625 31.296875 43.53125 35.828125 \nQ 39.65625 40.375 33.015625 40.375 \nz\nM 52.59375 71.296875 \nL 52.59375 62.3125 \nQ 48.875 64.0625 45.09375 64.984375 \nQ 41.3125 65.921875 37.59375 65.921875 \nQ 27.828125 65.921875 22.671875 59.328125 \nQ 17.53125 52.734375 16.796875 39.40625 \nQ 19.671875 43.65625 24.015625 45.921875 \nQ 28.375 48.1875 33.59375 48.1875 \nQ 44.578125 48.1875 50.953125 41.515625 \nQ 57.328125 34.859375 57.328125 23.390625 \nQ 57.328125 12.15625 50.6875 5.359375 \nQ 44.046875 -1.421875 33.015625 -1.421875 \nQ 20.359375 -1.421875 13.671875 8.265625 \nQ 6.984375 17.96875 6.984375 36.375 \nQ 6.984375 53.65625 15.1875 63.9375 \nQ 23.390625 74.21875 37.203125 74.21875 \nQ 40.921875 74.21875 44.703125 73.484375 \nQ 48.484375 72.75 52.59375 71.296875 \nz\n" id="DejaVuSans-54"/>\n      </defs>\n      <g transform="translate(277.877783 322.182437)scale(0.1 -0.1)">\n       <use xlink:href="#DejaVuSans-54"/>\n      </g>\n     </g>\n    </g>\n    <g id="xtick_5">\n     <g id="line2d_5">\n      <g>\n       <use style="stroke:#000000;stroke-width:0.8;" x="350.134468" xlink:href="#mf0090338b1" y="307.584"/>\n      </g>\n     </g>\n     <g id="text_5">\n      <!-- 8 -->\n      <defs>\n       <path d="M 31.78125 34.625 \nQ 24.75 34.625 20.71875 30.859375 \nQ 16.703125 27.09375 16.703125 20.515625 \nQ 16.703125 13.921875 20.71875 10.15625 \nQ 24.75 6.390625 31.78125 6.390625 \nQ 38.8125 6.390625 42.859375 10.171875 \nQ 46.921875 13.96875 46.921875 20.515625 \nQ 46.921875 27.09375 42.890625 30.859375 \nQ 38.875 34.625 31.78125 34.625 \nz\nM 21.921875 38.8125 \nQ 15.578125 40.375 12.03125 44.71875 \nQ 8.5 49.078125 8.5 55.328125 \nQ 8.5 64.0625 14.71875 69.140625 \nQ 20.953125 74.21875 31.78125 74.21875 \nQ 42.671875 74.21875 48.875 69.140625 \nQ 55.078125 64.0625 55.078125 55.328125 \nQ 55.078125 49.078125 51.53125 44.71875 \nQ 48 40.375 41.703125 38.8125 \nQ 48.828125 37.15625 52.796875 32.3125 \nQ 56.78125 27.484375 56.78125 20.515625 \nQ 56.78125 9.90625 50.3125 4.234375 \nQ 43.84375 -1.421875 31.78125 -1.421875 \nQ 19.734375 -1.421875 13.25 4.234375 \nQ 6.78125 9.90625 6.78125 20.515625 \nQ 6.78125 27.484375 10.78125 32.3125 \nQ 14.796875 37.15625 21.921875 38.8125 \nz\nM 18.3125 54.390625 \nQ 18.3125 48.734375 21.84375 45.5625 \nQ 25.390625 42.390625 31.78125 42.390625 \nQ 38.140625 42.390625 41.71875 45.5625 \nQ 45.3125 48.734375 45.3125 54.390625 \nQ 45.3125 60.0625 41.71875 63.234375 \nQ 38.140625 66.40625 31.78125 66.40625 \nQ 25.390625 66.40625 21.84375 63.234375 \nQ 18.3125 60.0625 18.3125 54.390625 \nz\n" id="DejaVuSans-56"/>\n      </defs>\n      <g transform="translate(346.953218 322.182437)scale(0.1 -0.1)">\n       <use xlink:href="#DejaVuSans-56"/>\n      </g>\n     </g>\n    </g>\n   </g>\n   <g id="matplotlib.axis_2">\n    <g id="ytick_1">\n     <g id="line2d_6">\n      <defs>\n       <path d="M 0 0 \nL -3.5 0 \n" id="mbd7003c97e" style="stroke:#000000;stroke-width:0.8;"/>\n      </defs>\n      <g>\n       <use style="stroke:#000000;stroke-width:0.8;" x="57.6" xlink:href="#mbd7003c97e" y="295.497285"/>\n      </g>\n     </g>\n     <g id="text_6">\n      <!-- −1.00 -->\n      <defs>\n       <path d="M 10.59375 35.5 \nL 73.1875 35.5 \nL 73.1875 27.203125 \nL 10.59375 27.203125 \nz\n" id="DejaVuSans-8722"/>\n       <path d="M 12.40625 8.296875 \nL 28.515625 8.296875 \nL 28.515625 63.921875 \nL 10.984375 60.40625 \nL 10.984375 69.390625 \nL 28.421875 72.90625 \nL 38.28125 72.90625 \nL 38.28125 8.296875 \nL 54.390625 8.296875 \nL 54.390625 0 \nL 12.40625 0 \nz\n" id="DejaVuSans-49"/>\n       <path d="M 10.6875 12.40625 \nL 21 12.40625 \nL 21 0 \nL 10.6875 0 \nz\n" id="DejaVuSans-46"/>\n      </defs>\n      <g transform="translate(19.954687 299.296504)scale(0.1 -0.1)">\n       <use xlink:href="#DejaVuSans-8722"/>\n       <use x="83.789062" xlink:href="#DejaVuSans-49"/>\n       <use x="147.412109" xlink:href="#DejaVuSans-46"/>\n       <use x="179.199219" xlink:href="#DejaVuSans-48"/>\n       <use x="242.822266" xlink:href="#DejaVuSans-48"/>\n      </g>\n     </g>\n    </g>\n    <g id="ytick_2">\n     <g id="line2d_7">\n      <g>\n       <use style="stroke:#000000;stroke-width:0.8;" x="57.6" xlink:href="#mbd7003c97e" y="265.249676"/>\n      </g>\n     </g>\n     <g id="text_7">\n      <!-- −0.75 -->\n      <defs>\n       <path d="M 8.203125 72.90625 \nL 55.078125 72.90625 \nL 55.078125 68.703125 \nL 28.609375 0 \nL 18.3125 0 \nL 43.21875 64.59375 \nL 8.203125 64.59375 \nz\n" id="DejaVuSans-55"/>\n       <path d="M 10.796875 72.90625 \nL 49.515625 72.90625 \nL 49.515625 64.59375 \nL 19.828125 64.59375 \nL 19.828125 46.734375 \nQ 21.96875 47.46875 24.109375 47.828125 \nQ 26.265625 48.1875 28.421875 48.1875 \nQ 40.625 48.1875 47.75 41.5 \nQ 54.890625 34.8125 54.890625 23.390625 \nQ 54.890625 11.625 47.5625 5.09375 \nQ 40.234375 -1.421875 26.90625 -1.421875 \nQ 22.3125 -1.421875 17.546875 -0.640625 \nQ 12.796875 0.140625 7.71875 1.703125 \nL 7.71875 11.625 \nQ 12.109375 9.234375 16.796875 8.0625 \nQ 21.484375 6.890625 26.703125 6.890625 \nQ 35.15625 6.890625 40.078125 11.328125 \nQ 45.015625 15.765625 45.015625 23.390625 \nQ 45.015625 31 40.078125 35.4375 \nQ 35.15625 39.890625 26.703125 39.890625 \nQ 22.75 39.890625 18.8125 39.015625 \nQ 14.890625 38.140625 10.796875 36.28125 \nz\n" id="DejaVuSans-53"/>\n      </defs>\n      <g transform="translate(19.954687 269.048894)scale(0.1 -0.1)">\n       <use xlink:href="#DejaVuSans-8722"/>\n       <use x="83.789062" xlink:href="#DejaVuSans-48"/>\n       <use x="147.412109" xlink:href="#DejaVuSans-46"/>\n       <use x="179.199219" xlink:href="#DejaVuSans-55"/>\n       <use x="242.822266" xlink:href="#DejaVuSans-53"/>\n      </g>\n     </g>\n    </g>\n    <g id="ytick_3">\n     <g id="line2d_8">\n      <g>\n       <use style="stroke:#000000;stroke-width:0.8;" x="57.6" xlink:href="#mbd7003c97e" y="235.002066"/>\n      </g>\n     </g>\n     <g id="text_8">\n      <!-- −0.50 -->\n      <g transform="translate(19.954687 238.801285)scale(0.1 -0.1)">\n       <use xlink:href="#DejaVuSans-8722"/>\n       <use x="83.789062" xlink:href="#DejaVuSans-48"/>\n       <use x="147.412109" xlink:href="#DejaVuSans-46"/>\n       <use x="179.199219" xlink:href="#DejaVuSans-53"/>\n       <use x="242.822266" xlink:href="#DejaVuSans-48"/>\n      </g>\n     </g>\n    </g>\n    <g id="ytick_4">\n     <g id="line2d_9">\n      <g>\n       <use style="stroke:#000000;stroke-width:0.8;" x="57.6" xlink:href="#mbd7003c97e" y="204.754457"/>\n      </g>\n     </g>\n     <g id="text_9">\n      <!-- −0.25 -->\n      <g transform="translate(19.954687 208.553676)scale(0.1 -0.1)">\n       <use xlink:href="#DejaVuSans-8722"/>\n       <use x="83.789062" xlink:href="#DejaVuSans-48"/>\n       <use x="147.412109" xlink:href="#DejaVuSans-46"/>\n       <use x="179.199219" xlink:href="#DejaVuSans-50"/>\n       <use x="242.822266" xlink:href="#DejaVuSans-53"/>\n      </g>\n     </g>\n    </g>\n    <g id="ytick_5">\n     <g id="line2d_10">\n      <g>\n       <use style="stroke:#000000;stroke-width:0.8;" x="57.6" xlink:href="#mbd7003c97e" y="174.506848"/>\n      </g>\n     </g>\n     <g id="text_10">\n      <!-- 0.00 -->\n      <g transform="translate(28.334375 178.306066)scale(0.1 -0.1)">\n       <use xlink:href="#DejaVuSans-48"/>\n       <use x="63.623047" xlink:href="#DejaVuSans-46"/>\n       <use x="95.410156" xlink:href="#DejaVuSans-48"/>\n       <use x="159.033203" xlink:href="#DejaVuSans-48"/>\n      </g>\n     </g>\n    </g>\n    <g id="ytick_6">\n     <g id="line2d_11">\n      <g>\n       <use style="stroke:#000000;stroke-width:0.8;" x="57.6" xlink:href="#mbd7003c97e" y="144.259238"/>\n      </g>\n     </g>\n     <g id="text_11">\n      <!-- 0.25 -->\n      <g transform="translate(28.334375 148.058457)scale(0.1 -0.1)">\n       <use xlink:href="#DejaVuSans-48"/>\n       <use x="63.623047" xlink:href="#DejaVuSans-46"/>\n       <use x="95.410156" xlink:href="#DejaVuSans-50"/>\n       <use x="159.033203" xlink:href="#DejaVuSans-53"/>\n      </g>\n     </g>\n    </g>\n    <g id="ytick_7">\n     <g id="line2d_12">\n      <g>\n       <use style="stroke:#000000;stroke-width:0.8;" x="57.6" xlink:href="#mbd7003c97e" y="114.011629"/>\n      </g>\n     </g>\n     <g id="text_12">\n      <!-- 0.50 -->\n      <g transform="translate(28.334375 117.810848)scale(0.1 -0.1)">\n       <use xlink:href="#DejaVuSans-48"/>\n       <use x="63.623047" xlink:href="#DejaVuSans-46"/>\n       <use x="95.410156" xlink:href="#DejaVuSans-53"/>\n       <use x="159.033203" xlink:href="#DejaVuSans-48"/>\n      </g>\n     </g>\n    </g>\n    <g id="ytick_8">\n     <g id="line2d_13">\n      <g>\n       <use style="stroke:#000000;stroke-width:0.8;" x="57.6" xlink:href="#mbd7003c97e" y="83.764019"/>\n      </g>\n     </g>\n     <g id="text_13">\n      <!-- 0.75 -->\n      <g transform="translate(28.334375 87.563238)scale(0.1 -0.1)">\n       <use xlink:href="#DejaVuSans-48"/>\n       <use x="63.623047" xlink:href="#DejaVuSans-46"/>\n       <use x="95.410156" xlink:href="#DejaVuSans-55"/>\n       <use x="159.033203" xlink:href="#DejaVuSans-53"/>\n      </g>\n     </g>\n    </g>\n    <g id="ytick_9">\n     <g id="line2d_14">\n      <g>\n       <use style="stroke:#000000;stroke-width:0.8;" x="57.6" xlink:href="#mbd7003c97e" y="53.51641"/>\n      </g>\n     </g>\n     <g id="text_14">\n      <!-- 1.00 -->\n      <g transform="translate(28.334375 57.315629)scale(0.1 -0.1)">\n       <use xlink:href="#DejaVuSans-49"/>\n       <use x="63.623047" xlink:href="#DejaVuSans-46"/>\n       <use x="95.410156" xlink:href="#DejaVuSans-48"/>\n       <use x="159.033203" xlink:href="#DejaVuSans-48"/>\n      </g>\n     </g>\n    </g>\n   </g>\n   <g id="line2d_15">\n    <path clip-path="url(#p74e91c0384)" d="M 73.832727 174.506848 \nL 77.286499 162.427959 \nL 80.740271 150.469758 \nL 84.194043 138.751728 \nL 87.647814 127.390952 \nL 91.101586 116.500942 \nL 94.555358 106.190508 \nL 98.00913 96.562668 \nL 101.462901 87.71362 \nL 104.916673 79.731782 \nL 108.370445 72.696905 \nL 111.824217 66.679279 \nL 115.277988 61.739031 \nL 118.73176 57.925521 \nL 122.185532 55.276854 \nL 125.639304 53.819493 \nL 129.093075 53.568 \nL 132.546847 54.524888 \nL 136.000619 56.680597 \nL 139.454391 60.013586 \nL 142.908162 64.490554 \nL 146.361934 70.066769 \nL 149.815706 76.686514 \nL 153.269478 84.283648 \nL 156.72325 92.782262 \nL 160.177021 102.097441 \nL 163.630793 112.136111 \nL 167.084565 122.797969 \nL 170.538337 133.976485 \nL 173.992108 145.559967 \nL 177.44588 157.432676 \nL 180.899652 169.475985 \nL 184.353424 181.569561 \nL 187.807195 193.592568 \nL 191.260967 205.424877 \nL 194.714739 216.948264 \nL 198.168511 228.04759 \nL 201.622282 238.611954 \nL 205.076054 248.535802 \nL 208.529826 257.719976 \nL 211.983598 266.072713 \nL 215.437369 273.510553 \nL 218.891141 279.959182 \nL 222.344913 285.354165 \nL 225.798685 289.641599 \nL 229.252456 292.778644 \nL 232.706228 294.733957 \nL 236.16 295.488 \nL 239.613772 295.033239 \nL 243.067544 293.374219 \nL 246.521315 290.527515 \nL 249.975087 286.521571 \nL 253.428859 281.396413 \nL 256.882631 275.20325 \nL 260.336402 268.003961 \nL 263.790174 259.87048 \nL 267.243946 250.884074 \nL 270.697718 241.134532 \nL 274.151489 230.719269 \nL 277.605261 219.742349 \nL 281.059033 208.313451 \nL 284.512805 196.546769 \nL 287.966576 184.559871 \nL 291.420348 172.472526 \nL 294.87412 160.405508 \nL 298.327892 148.479386 \nL 301.781663 136.813322 \nL 305.235435 125.523879 \nL 308.689207 114.723857 \nL 312.142979 104.521167 \nL 315.59675 95.017752 \nL 319.050522 86.308564 \nL 322.504294 78.480625 \nL 325.958066 71.612149 \nL 329.411838 65.771762 \nL 332.865609 61.01782 \nL 336.319381 57.397823 \nL 339.773153 54.947941 \nL 343.226925 53.692651 \nL 346.680696 53.644498 \nL 350.134468 54.80396 \nL 353.58824 57.159455 \nL 357.042012 60.687446 \nL 360.495783 65.352683 \nL 363.949555 71.108552 \nL 367.403327 77.897542 \nL 370.857099 85.651821 \nL 374.31087 94.29391 \nL 377.764642 103.73746 \nL 381.218414 113.888115 \nL 384.672186 124.644452 \nL 388.125957 135.898997 \nL 391.579729 147.539299 \nL 395.033501 159.449052 \nL 398.487273 171.509258 \n" style="fill:none;stroke:#1f77b4;stroke-linecap:square;stroke-width:1.5;"/>\n   </g>\n   <g id="patch_3">\n    <path d="M 57.6 307.584 \nL 57.6 41.472 \n" style="fill:none;stroke:#000000;stroke-linecap:square;stroke-linejoin:miter;stroke-width:0.8;"/>\n   </g>\n   <g id="patch_4">\n    <path d="M 414.72 307.584 \nL 414.72 41.472 \n" style="fill:none;stroke:#000000;stroke-linecap:square;stroke-linejoin:miter;stroke-width:0.8;"/>\n   </g>\n   <g id="patch_5">\n    <path d="M 57.6 307.584 \nL 414.72 307.584 \n" style="fill:none;stroke:#000000;stroke-linecap:square;stroke-linejoin:miter;stroke-width:0.8;"/>\n   </g>\n   <g id="patch_6">\n    <path d="M 57.6 41.472 \nL 414.72 41.472 \n" style="fill:none;stroke:#000000;stroke-linecap:square;stroke-linejoin:miter;stroke-width:0.8;"/>\n   </g>\n  </g>\n </g>\n <defs>\n  <clipPath id="p74e91c0384">\n   <rect height="266.112" width="357.12" x="57.6" y="41.472"/>\n  </clipPath>\n </defs>\n</svg>\n',
  date: new Date("2018-11-09T19:13:39.267Z"),
  output: true
};

export const Warning = {
  uid: "biZrI1J09",
  sessionId: "kUx3v2_W5",
  type: "<class 'UserWarning'>",
  pyType: "Warning",
  value: {
    type: "<class 'UserWarning'>",
    file: "None",
    lineno: 13,
    msg: "This is a warning",
    source: "None"
  },
  date: new Date("2018-11-09T19:13:39.267Z"),
  output: true
};

export const Exception = {
  uid: "PazD2U6AEl",
  sessionId: "kUx3v2_W5",
  type: "SyntaxError",
  pyType: "Exception",
  value: {
    type: "SyntaxError",
    tb: [
      "Traceback (most recent call last):\n",
      '  File "C:\\Temp\\yapijReVenv\\lib\\site-packages\\yapij\\context.py", line 40, in catch_output\n    yield\n',
      "  File \"C:\\Temp\\yapijReVenv\\lib\\site-packages\\yapij\\session.py\", line 126, in _done_callback\n    raise fut.result()['error']\n",
      '  File "C:\\Temp\\yapijReVenv\\lib\\site-packages\\yapij\\session.py", line 145, in exec_one_command\n    exec(code, {}, env)\n',
      '  File "<str>", line 1, in <module>\n',
      '  File "<string>", line None\n',
      "SyntaxError: This is an error!\n"
    ],
    args: ["This is an error!"],
    lineno: 40
  },
  date: new Date("2018-11-09T19:13:39.267Z"),
  output: true
};
```
