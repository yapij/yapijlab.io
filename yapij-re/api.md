# API

---

{% hint style='info' %}
Information about react components that are provided with `yapij-re` can be found in [Pre-Built React Components](prebuilt-react-components.md).
{% endhint %}

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

---

# `<YapijProvider />`

Coordinates communication between python processes, redux store, and connected components.

This component initiates a redux store. Flexibility is provided for incorporating other reducers and middleware into the store. Thus, this component will replace redux's `Provider` component (in fact, it wraps the `Provider` component).

## Arguments

- `reducers: PropTypes.object` (Optional=`{}`). Other reducers to include in the app's redux store.
- `preloadedState: PropTypes.any` (Optianal=`null`). Passed to `redux.createStore`. See [redux docs](https://redux.js.org/api/createstore#arguments) for more information.
- `enhancer: PropTypes.func` (Optional=`null`). Passed to `redux.createStore`. See [redux docs](https://redux.js.org/api/createstore#arguments) for more information.
- `servers: PropTypes.oneOf([PropTypes.arrayOf(PropTypes.string), PropTypes.string])` (Optional=`"default"`). Name(s) of server(s) to be created by the component. If an array is passed, all names must be unique. See the [FAQ](yapij-re/faq) for more advice about one v. multiple servers.
- `yapijConfig: PropTypes.objectOf(PropTypes.object)` (Optional=`{ pythonArgs: null, config: null }`). Configuration options passed to the `yapij-js` server instances.
- `yapijKeys: PropTypes.objectOf(PropTypes.string)` (Optional=`{redux: yapijReduxKey, pubsub: yapijPubSubKey }`). Specialized keys to be used for the redux store and [`pubsub-js`](https://www.npmjs.com/package/pubsub-js). Generally, these should not be specified manually.

# Connectors

{% hint style='warning' %}
These elements are only relevant for users who wish to create their own components for handling yapij output. For more information, see [Development](yapij-re/development)
{% endhint %}

Connectors are [higher-order react components](https://reactjs.org/docs/higher-order-components.html) that can be be used to create react components that issue commands to the yapij server or connect to the yapij server output.

## Methods

### `connectCurator`

A factory for generating yapij curators, which provide output from an underlying server.

#### Signature

```js
connectCurator(name, propNames)(WrappedComponent);
```

#### Arguments

- `name:[null, string]` (Optional=`'default'`). The name of the server from which the component should receive output. All servers named be specified in `YapijProvider`.
  - If `name=null`, output from server `default` is received.
  - If `name="server1"`, only output emanating from `server1` are received.
- `propNames: object` (Optional=`{ methods: "yapij", collection: "collection" }`). Names of props that should be passed to the `WrappedComponent`. You only need to change these if you have a prop with a conflicting name. The props are:
  - `yapij:object`. Methods for controlling the server. See below.
  - `collection:Array`. Array of process output.

#### Example

```js
import React from "react";
import { connectCurator } from "yapij-re";

class SimpleBeacon extends React.Component {
  ...
}

export default connectYapijBeacon(
  (name = "default"),
  (propNames = { methods: "yapij", msg: "msg" })
)(SimpleBeacon);
```

### `connectYapijBeacon`

A factory for generating yapij beacons, which provide information about the underlying process (e.g. start, stop, stderr, etc.).

#### Signature

```js
connectYapijBeacon(name, propNames)(WrappedComponent);
```

#### Arguments

- `name:[null, string, Array]` (Optional=`null`). The name(s) of the server(s) from which the component should receive messages. All servers named be specified in `YapijProvider`.
  - If `name=null`, all server messages are received.
  - If `name="server1"`, only messages emanating from `server1` are received.
  - If `name=["default", "server1"]`, only messages emanating from processes `default` and `server1` are received.
- `propNames: object` (Optional=`{ methods: "yapij", msg: "msg" }`). Names of props that should be passed to the `WrappedComponent`. You only need to change these if you have a prop with a conflicting name.
  - `yapij:object`. Methods for controlling the server. See below.
  - `msg:Array`. Array of process messages passed by processes specified in `name` argument.

#### Example

```js
import React from "react";
import { connectCurator } from "yapij-re";

class SimpleBeacon extends React.Component {
  ...
}

export default connectYapijBeacon(
  (name = "default"),
  (propNames = { methods: "yapij", msg: "msg" })
)(SimpleBeacon);
```

## Example

```js
import React from "react";
import { connectCurator } from "yapij-re";

class SimpleCurator extends React.Component {
  componentDidMount() {
    this.props.yapij.on("ready", (isReady, err) => {
      if (err) throw err;
      console.log(`Process is ${isReady ? "" : "not"} ready.`);
    });
  }

  render() {
    return (
      <table>
        <tbody>
          {this.props.collection.map((c, ii) => (
            <tr key={`row-${ii}`}>
              <td>{c.uid}</td>
              <td>{c.value.toString()}</td>
            </tr>
          ))}
        </tbody>
        <tfoot>
          {this.props.yapij.ready((isReady, err) => {
            return err ? `${err}` : `Process is ${isReady ? "" : "not"} ready.`;
          })}
          ;
        </tfoot>
      </table>
    );
  }
}

export default connectCurator(
  (name = "default"),
  (propNames = { methods: "yapij", collection: "collection" })
)(SimpleCurator);
```

## Tips

### Keys in `this.props.yapij`

#### `start(callback)`

Start a python session on the server. If a session is already started nothing will occur.

{% hint style='info' %}
No callback is made until:

1. The python process has started
1. ZMQ communications on the node side are set up.
1. A heartbeat from the python process is received.

In short, not callback is made until the process is really ready to start executing commands.
{% endhint %}

_Arguments._

- `callback:func` (Optional). Callback to be called at the end of this method. Callback should have arguments:
  - `server:object`. The base server from `yapij-js`
  - `ports:object`. The tcp ports used to communicate with the python process.
  - `e:Error`. If the command is unsuccessful an error will be passed.

#### `stop(callback)`

Stop the python session on the server. If a session is not already running nothing will occur.

_Arguments._

- `callback:func` (Optional). Callback to be called at the end of this method. Callback should have arguments:
  - `server:object`. The base server from `yapij-js`
  - `ports:object`. The (now-closed) tcp ports used to communicate with the python process.
  - `e:Error`. If the command is unsuccessful an error will be passed.

#### `restart(callback)`

Restart the python session on the server. If a session is not already running, session will start.

_Arguments._

- `callback:func` (Optional). Callback to be called at the end of this method. Callback should have arguments:
  - `server:object`. The base server from `yapij-js`
  - `ports:object`. The tcp ports used to communicate with the python process.
  - `e:Error`. If the command is unsuccessful an error will be passed.

#### `ready(verbose, callback)`

Check to see if the python session is operative. Checks both for existence of python process and that ZMQ communications on node side are operative.

_Arguments._

- `verbose:bool` (Optional=`true`). If true, the result of the ready command will be posted to `serverMsg` and picked up by beacons.
- `callback:func` (Optional). Callback to be called at the end of this method. Callback should have arguments:
  - `isReady:boolean`. If `true`, python process is ready to process commands.
  - `e:Error`. If the command is unsuccessful an error will be passed.

#### `exec(cmd, callback)`

Send a command to the python process. May be either a true python command or a magic.

_Arguments._

- `cmd:string`. Python command.
- `callback:func` (Optional). Callback to be called at the end of this method. Callback should have arguments:
  - `handle:cmdHandler`. A command handler.
  - `e:Error`. If the command is unsuccessful an error will be passed.

#### `interrupt(callback)`

Raise a `KeyboardInterrupt` exception in the python process.

_Arguments._

- `callback:func` (Optional). Callback to be called at the end of this method. Callback should have arguments:
  - `e:Error`. If the command is unsuccessful an error will be passed.

#### `on(method, onCallback, callback)`.

Register a callback to be called every time that argument `method` is called. An alternative to passing a callback each time pass an argument.

{% hint style='info' %}
Any call to `this.props.yapij.on(...)` must occur in the `componentDidMount` method.
{% endhint %}

_Arguments._

- `method:string`. One of ['start', 'stop', 'restart', 'ready', 'exec', 'interrupt'].
- `onCallback:func`. Actual callback to be called each time `on` method is called.
- `callback:func` (optional). Callback to be called at the end of this method (i.e. `on`).

# `YapijServer`

{% hint style='warning' %}
These elements are only relevant for developers. Most users will only need [Pre-built React Components](yapij-re/prebuilt-react-components) or (at most) [Connectors](#connectors)
{% endhint %}

TBD

# Redux Objects

{% hint style='warning' %}
These elements are only relevant for developers. Most users will only need [Pre-built React Components](yapij-re/prebuilt-react-components) or (at most) [Connectors](#connectors)
{% endhint %}

## `yapijActions`

- `createNewServer(serverName, server)`.
- `startKernel(server)`.
- `stopKernel(server)`.
- `restartKernel(server)`.
- `createNewCmd(server, cmd)`.
- `appendOutputCmd(server, out)`.
- `appendStatusCmd(server, out)`.
- `createServerMsg(server, msg, intent = 0)`.

## `yapijReducers`

The reducer that is passed to `redux.createStore`.

## `yapijTopics`

Topics specify actions that can be taken to modify the yapij part of the redux store.

- Kernel Actions
  - `NEW_SERVER`. Creates space for new server output.
  - `START_KERNEL`.
  - `STOP_KERNEL`.
  - `RESTART_KERNEL`.
- Execution Related
  - `NEW_CMD`. Called whenever a new command is generated. Includes information like the command, unique id, and date.
  - `APPEND_OUTPUT_CMD`. Adds output from process to store.
  - `APPEND_STATUS_CMD`. Adds status from process to store.
- Server status
  - `ADD_SERVER_MSG`. Adds a server message
