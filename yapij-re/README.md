# yapij-re

React+Redux components for plugging into the lower-level `yapij` package.

---

`yapij-re` provides a wrapper for creating the redux store and a set of react components that interact with the store.

This script creates a minimal react app that can be plugged into an electron app:

```js
import React from 'react';
import ReactDOM from 'react-dom';

import {YapijProvider, Shell, YapijBeacon} from 'yapij-re';

const App = () => (
  <YapijProvider>
    <Shell />
    <YapijBeacon />
  <\/YapijProvider>
);

ReactDOM.render(<Index />, document.getElementById("root"));
```

What is happening here?

- `YapijProvider` coordinates communication between:
  1. Python
  1. The redux store
  1. Child components (namely, `Shell`).
- `Shell` is a pre-built repl editor for python.
  - It sends commands to `YapijProvider` via a pub-sub pattern.
  - It receives a prop `collection`, which contains all output from the python session.
- `YapijBeacon` is a pre-built component that provides information about the state of the python process.
  - It sends commands to `YapijProvider` via a pub-sub pattern.
  - It receives a prop `msg`, which registers messages when the process starts, stops, or closes unexpectedly.
