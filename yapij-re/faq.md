# FAQ

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

**Table of Contents** _generated with [DocToc](https://github.com/thlorenz/doctoc)_

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

---

# When is more than one server necessary?

You might want more than one server if you have two distinct python processes that you'd like to run. For example, you could have a "default" process for dealing with catch-all issues and a "data" server to deal specifically with data handling.

A cautions concerning the choice of servers:

- You should _not_ use multiple servers to do parallel processing (use python's built-in `multiprocessing` module instead).
- You can carry out "administrative"/magic tasks while computing on a server (e.g. rename a workspace while running a long python script).
- Server workspaces are not shared.
- Do not try to load and manipulate the same workspace from two servers at once as this will create a race condition.

A good use case for multiple servers arises when you want to have two processes in the UI that run separate "major" tasks. For example, if you want to use one server to be a catch-all for a main process (e.g. user's command-line input) while another server runs separate tasks that run at the same time as the other process. For example, this second task could involve fetching data via a python API.

---

# What are the codes for `serverMsg` intents?

- `0`: default
- `1`: info
- `2`: warning
- `3`: danger
- `4`: success
