# Installation

```bash
npm i yapij-re
```

You will also need to configure `yapij-js` (a dependency). See [Installing `yapij-js`](/yapij-js/install).
