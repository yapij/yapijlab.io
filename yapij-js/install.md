# Installing `yapij-js`

## With Wizard

_(Coming soon.)_

## Manually

These instructions are geared towards those who are using `yapij-js` in a development context. Ensuring that your end users are able to easily install and run the package is (unfortunately) another matter.

### 0. Check for Dependencies

`yapij-js` requires:

- node
- python>=3.5

### 1. Install the node package

```bash
npm i yapij-js
```

### 2. Set up a python virtual environment

Now set up a virtual environment that python can run in. It might be a good idea to put this venv some place where it won't accidentally be deleted or published while using npm.

#### Windows

You might use something like `C:\Temp`:

```bash
cd C:\Temp
python -m venv yapijReVenv
cd yapijReVenv
Scripts\activate
```

#### Mac/Unix

```bash
cd /tmp
python3 -m venv yapijReVenv
cd yapijReVenv
bin/activate
```

### 3. Download the latest version of `yapij-py`

Ensure that you are in a shell where the virtualenv is activated. This package is stored on the PyPri system:

```bash
pip install yapij-py -i https://api.python-private-package-index.com/0kjsWbi9m/
```

You may need to install some dependencies:

```bash
pip install pyzmq msgpack dill numpy pandas
```

{% hint style='warning' %}
Make this an automatic install in `setup.py`.
{% endhint %}

### 4. Update the `config` file for `yapij-js`

```js
const {config} = require('yapij-js');

// Path to virtualenv + a name to remember it by
var venv = {path: 'C:\\Temp\\yapijReVenv\\Scripts\\python.exe', name: 'default'};

config.set('python.venvDirs', [venv]);  // Add to list of possible
config.set('python.defaultVenv', venv); // Make it the default

// Information about the python version used.
config.set('python.python', {exec: 'python', v: '3.7'});

// Additional Spawn Commands (if applicable). See:
// https://nodejs.org/api/child_process.html#child_process_child_process_spawn_command_args_options
config.set('spawn', ...)

config.set('installed', true); // Package is now installed (DEPRECATED?)
```
