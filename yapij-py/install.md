# Installation

```bash
pip install yapij-py -i https://api.python-private-package-index.com/0kjsWbi9m/
```

When prompted, use the username and password that you were issued.

# Upgrading

```bash
pip install yapij-py -U -i https://api.python-private-package-index.com/0kjsWbi9m/
```
