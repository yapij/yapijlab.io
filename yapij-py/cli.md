# CLI

The entry point of `yapij-py` is a function, `run`, which parses command line input and starts up the threads. Here is the full content of that module:

```python
# yapij/run.py
import os
from parse import parse

from client import Client

def run():
  args = parse()
  pt = Client(
    ports=args['ports'],
    cmd_opts=args['cmd_opts'],
    heartbeat_interval=args['heartbeat_interval'],
    key=args['key']
  )
  pt.start()
  pt.join()

if __name__ == '__main__':
  run()
```

For more information about the command line arguments, see the full spec below.

# What happens inside of `yapij-js`

`yapij-js` comes packed with a python script that simply calls `yapij.run`:

```python
# yapij-js/src/static/run.py
import yapij

yapij.run();
```

Under the hood

```bash
python /path/to/yapij-js/src/static/run.py --frontend tcp://127.0.0.1:3001 --backend tcp://127.0.0.1:3002 --heartbeat_port tcp://127.0.0.1:3001 --heartbeat_interval 120 --key afa24
```

If we were using a virtual environment we'd run it from the venv's python. For example, in Windows this could be:

```bash
C:/Temp/yapijReVenv/Scripts/python.exe C:/Users/us57144/Dropbox/grantThornton/projects/macro_model/yapij/yapij-re/node_modules/yapij-js/src/static/run.py --frontend tcp://127.0.0.1:3001 --backend tcp://127.0.0.1:3002 --heartbeat_port tcp://127.0.0.1:3001 --heartbeat_interval 120 --key afa24
```

# Full CLI Spec

```bash
usage: run.py [-h] [--backend BACKEND] [--frontend FRONTEND]
              [--heartbeat_port HEARTBEAT_PORT]
              [--heartbeat_interval HEARTBEAT_INTERVAL] [-f INIT_FILENAME]
              [--user USER] [-s STARTUP_SCRIPT] [-k KEY] [-cd CD]

YAPIJ: Interactive Python from Node [Python side]

optional arguments:
  -h, --help            show this help message and exit
  --backend BACKEND, -bp BACKEND
                        Ports on which to connect for backend. (Required)
  --frontend FRONTEND, -fp FRONTEND
                        Ports on which to connect for frontend. (Required)
  --heartbeat_port HEARTBEAT_PORT, -hp HEARTBEAT_PORT
                        Ports on which to connect for heartbeat (Required)
  --heartbeat_interval HEARTBEAT_INTERVAL, -heart HEARTBEAT_INTERVAL
                        Seconds to wait between sending heartbeats. (Required,
                        default=120)
  -f INIT_FILENAME, --init_filename INIT_FILENAME
                        Filename of environment at initialization. If filename
                        exists, will load the file. If it does not, will
                        initialize a new environment with that filename.
                        (default: None)
  --user USER, -u USER  Name of user. (Default: Computer profile login name)
  -s STARTUP_SCRIPT, --startup_script STARTUP_SCRIPT
                        Code to be run once the environment is created.
                        (default: '')
  -k KEY, --key KEY     Key that uniquelly identifies/links this key with its
                        parent node process.
  -cd CD, --cd CD       Key that uniquelly identifies/links this key with its
                        parent node process.
```

You can always retrieve this spec by calling:

```bash
python /path/to/yapij-js/src/static/run.py -h
```
