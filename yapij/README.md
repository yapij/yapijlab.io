# `yapij`

`yapij` is a library of packages for running python from a node-based desktop application. It includes flexible

{% hint style='danger' %}
Never use `yapij` in an application that is exposed to the web. The whole point of yapij is to execute _arbitrary_ Python code. If the application is exposed to the web, users could execute bad stuff like `rm *`.
{% endhint %}

---

`yapij` is really a set of node and python packages:

​- [`yapij-re`](yapij-re). High-level React+Redux components for using yapij in an app.
​- [`yapij-js`](yapij-js). Low-level node module to communicate between node and python processes.

- [`yapij-py`](yapij-py). Low-level python package that is handled by yapij-js and that handles interprocess communication.

---

{% hint style='warning' %}
`yapij` is still very much a work in progress.
{% endhint %}
